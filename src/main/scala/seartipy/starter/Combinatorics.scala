package seartipy.starter

object Combinatorics {
  def factorial(n: Int): Int = {
    require(n >= 0)
    (1 to n).product
  }
}
