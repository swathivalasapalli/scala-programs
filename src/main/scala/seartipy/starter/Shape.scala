package seartipy.starter

sealed trait Shape

case class Rectangle(height: Int, width: Int) extends Shape
case class Circle(radius: Int) extends Shape
object ShapeUtils {

  def area(shape: Shape): Double = shape match {
    case Rectangle(height, width) => height * width
    case Circle(radius) => Math.PI * radius * radius
  }

  def perimeter(shape: Shape): Double = shape match {
    case Rectangle(height, width) => 2 * (height + width)
    case Circle(radius) => 2 * Math.PI * radius
  }
}




