package seartipy.starter

class ArrayList {
  private var arr: Array[Int] = Array(20)
  private var length: Int = 0

  def first: Int = arr(0)

  def last: Int = arr(length - 1)

  def isEmpty: Boolean = length == 0

  def size: Int = length

  def add(value: Int): Unit = {
    ensureCapacity(arr.length)
    arr(length) = value
    length += 1
  }

  def pop(): Int = {
    require(length != 0)
    length -= 1
    arr(length)
  }

  def copy(src: Array[Int], dest: Array[Int]): Unit = {
    for (i <- src.indices) {
      dest(i) = src(i)
    }
  }

  def get(index: Int): Int = {
    arr(index)
  }

  def set(index: Int, value: Int): Unit = {
    arr(index) = value
  }

  def ensureCapacity(newCapacity: Int): Unit = {
    if (arr.length > newCapacity) {
      return
    }
    else {
      val capacity = arr.length
      val temp = Array[Int](Math.max(newCapacity, capacity * 2 + 1))
      copy(arr, temp)
      arr = temp
    }
  }

  def insertAt(value: Int, at: Int) {
    ensureCapacity(length + 1)
    for (i <- length until at + 1) {
      arr(i) = arr(i - 1)
    }
    arr(at) = value
    length += 1
  }
}

