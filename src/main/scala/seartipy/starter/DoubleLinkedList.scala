package seartipy.starter

class DoubleNode(var data: Int, var next: DoubleNode, var prev: DoubleNode)

class DoubleLinkedList {
  private var head: DoubleNode = null
  private var tail: DoubleNode = null
  private var length: Int = 0

  def size(): Int = length
  def first(): Int = head.data
  def last(): Int = tail.data

  def firstNode(): DoubleNode = head

  def lastNode(): DoubleNode = tail

  def addFirst(value: Int): Unit = {
    val node = new DoubleNode(value, head, null)
    val first = head
    if (first != null) {
      first.prev = node
      head = node
    } else {
      head = node
      tail = node
    }
    length += 1
  }

  def addLast(value: Int):Unit = {
    val node = new DoubleNode(value, null, tail)
    val last = tail
    if (last != null) {
      last.next = node
      tail = node
    } else {
      head = node
      tail = node
    }
    length += 1
  }

  def nodeAt(index: Int): DoubleNode = {
    var temp = head
    var i = 0
    while (temp != null) {
      if (i == index) {
        temp
      }
      i += 1
      temp = temp.next
    }
    null
  }

  def insert(value: Int, at: DoubleNode): Unit = {
    val node = new DoubleNode(value, at, at.prev)
    if(at.prev != null){
      at.prev.next = node
      at.prev = node
    }
    if (at == head) {
      head = node
    }
    length += 1
  }

}