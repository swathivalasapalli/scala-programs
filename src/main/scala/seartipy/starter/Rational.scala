package seartipy.starter

class Rational(num: Int, den: Int) {
  require(den != 0)
  private val g = gcd(num, den)
  val numerator: Int = num / g
  val denominator: Int = den / g

  def +(that: Rational): Rational =
    new Rational(numerator * that.denominator + denominator * that.numerator,
                 denominator * that.denominator)

  def -(that: Rational): Rational =
    new Rational(numerator * that.denominator - denominator * that.numerator,
                 denominator * that.denominator)

  def *(that: Rational): Rational =
    new Rational(numerator * that.numerator, denominator * that.denominator)

  def /(that: Rational): Rational =
    new Rational(numerator * that.denominator, denominator * that.numerator)
}
