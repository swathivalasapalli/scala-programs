package seartipy.starter

class Complex(val real: Rational, val imaginary: Rational) {

  def +(that: Complex): Complex =
    new Complex(real + that.real, imaginary + that.imaginary)

  def -(that: Complex): Complex =
    new Complex(real - that.real, imaginary - that.imaginary)

  def *(that: Complex): Complex =
    new Complex(real * that.real, imaginary * that.imaginary)

  def /(that: Complex): Complex = {
    val den = real * that.real - imaginary * that.imaginary
    val numerator = (real * that.real + imaginary * that.imaginary) / den
    val denominator = (imaginary * that.real - real * that.imaginary) / den
    new Complex(numerator, denominator)
  }

}