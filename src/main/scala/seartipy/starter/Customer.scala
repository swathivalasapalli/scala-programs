package seartipy.starter

case class Customer(id: Int, name: String, address: Address, account: Account)