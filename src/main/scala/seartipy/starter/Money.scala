package seartipy.starter

class Money(_dollar: Int = 0, _cent: Int = 0) {

  private def this(totalcents: Int) = this(totalcents / 100, totalcents % 100)
  private val totalcents = dollar * 100 + cent

  val dollar: Int = totalcents / 100
  val cent: Int = totalcents % 100

  def +(that: Money): Money = new Money(totalcents + that.totalcents)
  def -(that: Money): Money = new Money(totalcents - that.totalcents)
  def *(n: Int): Money = new Money(totalcents * n)
  def /(n: Int): Money = new Money(totalcents / n)
  def compareTo(that: Money): Int = totalcents - that.totalcents

}
