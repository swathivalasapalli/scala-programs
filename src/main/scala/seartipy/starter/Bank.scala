package seartipy.starter

class Bank {
  private var customers =  Map.empty[Int, Customer]
  private var accounts = Map.empty[Int, Account]
  private var nextCustomerId = 1
  private var nextAccountId = 1

  def createCustomer(name: String, address: Address): Int = {
    val customer = Customer(nextCustomerId, name, address)
    customers = customers + (nextCustomerId -> customer)
    nextCustomerId += 1
    customer.id
  }

  def createAccount(customerId: Int, initialAmount: Money): Int = {
    val account = new Account(nextAccountId, initialAmount, customerId)
    accounts = accounts + (nextAccountId -> account)
    nextAccountId += 1
    account.id
  }

  def checkBalance(accountId: Int): Money = {
    accounts(accountId).getBalance
      }

  def withdraw(amount: Money, accountId: Int): Boolean = {
    accounts(accountId).withdrawal(amount)
  }

  def deposit(amount: Money, accountId: Int): Unit = {
    accounts(accountId).deposit(amount)
  }

}
