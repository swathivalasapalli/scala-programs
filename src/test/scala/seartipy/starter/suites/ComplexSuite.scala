package seartipy.starter.suites

import seartipy.starter.Complex
import seartipy.starter.Rational

import org.scalatest.{FunSuite, Matchers}

class ComplexSuite extends FunSuite with Matchers {

  test("constructor") {
    val c1 = new Complex(new Rational(1, 2), new Rational(1, 2))
    val c2 = new Complex(new Rational(1, 2), new Rational(1, 2))
    val c3 = c1 + c2
    assert(c3.real.numerator === 1 )
    assert(c3.real.denominator === 1)
    assert(c3.imaginary.numerator === 1)
    assert(c3.imaginary.denominator === 1)

    val c4 = new Complex(new Rational(1, 2), new Rational(2, 3))
    val c5 = new Complex(new Rational(1, 2), new Rational(1, 2))
    val c6 = c4 + c5
    assert(c6.real.numerator === 1)
    assert(c6.real.denominator === 1)
    assert(c6.imaginary.numerator === 7)
    assert(c6.imaginary.denominator === 6)
  }

  test("add") {
    val c1 = new Complex(new Rational(1, 2), new Rational(2, 3))
    val c2 = new Complex(new Rational(1, 2), new Rational(1, 2))
    val c3 = c1 + c2
    assert(c3.real.numerator === 1)
    assert(c3.real.denominator === 1)
    assert(c3.imaginary.numerator === 7)
    assert(c3.imaginary.denominator === 6)

    val c4 = new Complex(new Rational(1, 2), new Rational(1, 2))
    val c5 = new Complex(new Rational(1, 2), new Rational(1, 2))
    val c6 = c4 + c5
    assert(c6.real.numerator === 1 )
    assert(c6.real.denominator === 1)
    assert(c6.imaginary.numerator === 1)
    assert(c6.imaginary.denominator === 1)

  }

  test("subtract") {
    val c1 = new Complex(new Rational(5, 2), new Rational(3, 2))
    val c2 = new Complex(new Rational(3, 2), new Rational(1, 2))
    val c3 = c1 - c2
    assert(c3.real.numerator === 1)
    assert(c3.real.denominator === 1)
    assert(c3.imaginary.numerator === 1)
    assert(c3.imaginary.denominator === 1)


    val c4 = new Complex(new Rational(3, 2), new Rational(4, 3))
    val c5 = new Complex(new Rational(1, 2), new Rational(1, 2))
    val c6 = c4 -c5
    assert(c6.real.numerator === 1)
    assert(c6.real.denominator === 1)
    assert(c6.imaginary.numerator === 5)
    assert(c6.imaginary.denominator === 6)
  }

  test("multiply") {
    val c1 = new Complex(new Rational(5, 2), new Rational(3, 2))
    val c2 = new Complex(new Rational(3, 2), new Rational(1, 2))
    val c3 = c1 * c2
    assert(c3.real.numerator === 15)
    assert(c3.real.denominator === 4)
    assert(c3.imaginary.numerator === 3)
    assert(c3.imaginary.denominator === 4)


    val c4 = new Complex(new Rational(1, 2), new Rational(1, 2))
    val c5 = new Complex(new Rational(1, 2), new Rational(1, 2))
    val c6 = c4 * c5
    assert(c6.real.numerator === 1)
    assert(c6.real.denominator === 4)
    assert(c6.imaginary.numerator === 1)
    assert(c6.imaginary.denominator === 4)
  }

  test("divide") {
    val c1 = new Complex(new Rational(5, 2), new Rational(3, 2))
    val c2 = new Complex(new Rational(3, 2), new Rational(1, 2))
    val c3 = c1 / c2
    assert(c3.real.numerator === 5)
    assert(c3.real.denominator === 3)
    assert(c3.imaginary.numerator === 3)
    assert(c3.imaginary.denominator === 1)
  }
}