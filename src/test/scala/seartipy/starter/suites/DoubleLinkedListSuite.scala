package seartipy.starter.suites

import org.scalatest.{FunSuite, Matchers}
import seartipy.starter.DoubleLinkedList

class DoubleLinkedListSuite extends FunSuite with Matchers {

  test("addfirst") {
    val dl = new DoubleLinkedList()
    assert(dl.size() === 0)
    dl.addFirst(10)
    assert(dl.size() === 1)
    dl.addFirst(20)
    assert(dl.size() == 2)
    assert(dl.first() === 20)
    assert(dl.last() === 10)
    dl.addFirst(30)
    assert(dl.size() === 3)
    assert(dl.first() === 30)
    assert(dl.last() === 10)
    assert(dl.firstNode().data === 30)
    assert(dl.lastNode().data === 10)

  }

  test("addlast") {
    val dl = new DoubleLinkedList()
    assert(dl.size() === 0)
    dl.addFirst(10)
    assert(dl.size() === 1)
    dl.addLast(20)
    assert(dl.size() === 2)
    assert(dl.first() === 10)
    assert(dl.last() === 20)
  }

  test("node at") {
    val dl = new DoubleLinkedList()
    assert(dl.size() === 0)
    dl.addFirst(10)
    assert(dl.size() === 1)
    assert(dl.nodeAt(0).data === 10)
    dl.addFirst(20)
    assert(dl.size() === 2)
    assert(dl.nodeAt(0).data === 20)
    assert(dl.nodeAt(1).data === 10)

  }

  test("insertat") {
    val dl = new DoubleLinkedList()
    assert(dl.size() === 0)
    dl.addFirst(10)
    assert(dl.size() === 1)

 }

}