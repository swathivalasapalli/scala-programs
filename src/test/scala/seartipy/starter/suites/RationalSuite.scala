package seartipy.starter.suites

import seartipy.starter.Rational
import org.scalatest.{FunSuite, Matchers}

class RationalSuite extends FunSuite with Matchers {

  test("constructor") {
    val r = new Rational(1, 2)
    assert(r.numerator === 1)
    assert(r.denominator === 2)


    val proper = new Rational(6, 8)
    assert(proper.numerator === 3)
    assert(proper.denominator === 4)
  }

  test("add") {
    val r1 = new Rational(1, 2)
    val r2 = new Rational(3, 4)
    val r3 = r1 + r2
    assert(r3.numerator === 5)
    assert(r3.denominator === 4)

    val r4 = new Rational(0, 1)
    val r5 = new Rational(0, 1)
    val r6 = r4 + r5
    assert(r6.numerator === 0)
    assert(r6.denominator === 0)

  }

  test("subtract") {
    val r1 = new Rational(3, 2)
    val r2 = new Rational(1, 2)
    val r3 = r1 - r2
    assert(r3.numerator ===  1)
    assert(r3.denominator ===  1)

    val r4 = new Rational(5, 4)
    val r5 = new Rational(1, 2)
    val r6 = r4 - r5
    assert(r6.numerator === 3)
    assert(r6.denominator === 4)
  }

  test("multiply") {
    val r1 = new Rational(1, 2)
    val r2 = new Rational(1, 2)
    val r3 = r1 * r2
    assert(r3.numerator === 1)
    assert(r3.denominator === 4)


    val r4 = new Rational(1, 2)
    val r5 = new Rational(3, 4)
    val r6 = r4 * r5
    assert(r6.numerator === 3)
    assert(r6.denominator === 8)
  }

  test("division") {
    val r1 = new Rational(1, 2)
    val r2 = new Rational(3, 4)
    val r3 = r1 / r2
    assert(r3.numerator === 2)
    assert(r3.denominator === 3)
  }
}